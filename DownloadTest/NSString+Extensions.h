//
//  NSString+Extensions.h
//  Deploy
//
//  Created by Erik Nielsen on 2/2/14.
//  Copyright (c) 2014 Erik Nielsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

- (BOOL)exists;
+ (NSString *)randomString:(int)length strength:(int)strength;
// @source http://benscheirman.com/2010/04/handy-categories-on-nsstring/
- (BOOL)isBlank;
- (BOOL)contains:(NSString *)string;
- (NSArray *)splitOnChar:(char)ch;
- (NSString *)substringFrom:(NSInteger)from to:(NSInteger)to;
- (NSString *)stringByStrippingWhitespace;

@end
