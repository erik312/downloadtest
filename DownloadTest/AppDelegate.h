//
//  AppDelegate.h
//  DownloadTest
//
//  Created by Clique Studios on 1/28/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSURLDownloadDelegate>
{
    NSURLResponse *downloadResponse;
    int bytesReceived;
    IBOutlet NSTextField *progressTextField;
}

@property float progress;
@property id delegate;

@property (nonatomic) NSURLResponse *downloadResponse;
- (IBAction)unzipWordpress:(id)sender;
//- (IBAction)startDownloadingURL:(id)sender;

@end
