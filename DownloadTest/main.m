//
//  main.m
//  DownloadTest
//
//  Created by Clique Studios on 1/28/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
