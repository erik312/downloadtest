//
//  FileHandler.h
//  DownloadTest
//
//  Created by Erik Nielsen on 2/3/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileHandler : NSObject

+ (BOOL)directoryExistsAtPath:(NSString *)aPath;
+ (void)createNewDirectory:(NSString *)dirPath;

@end
