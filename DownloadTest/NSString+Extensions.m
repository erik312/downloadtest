//
//  NSString+Extensions.m
//  Deploy
//
//  Created by Erik Nielsen on 2/2/14.
//  Copyright (c) 2014 Erik Nielsen. All rights reserved.
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)

- (BOOL)exists
{
    if( ! self || [self isEqualToString:@""] || self.length == 0 )
        return false;
    return true;
}

+ (NSString *)randomString:(int)length strength:(int)strength
{
    NSMutableString *output = [NSMutableString stringWithCapacity:length];
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSString *numbers = @"0123456789";
    NSString *characters = @"~!@#$%^&*()_-=+";
    NSArray *types = @[letters, numbers, characters];
    u_int32_t levelNum = 0;
    for(int i = 0; i < length; i++) {
        if(strength > 0)
            levelNum = arc4random() % (strength + 1);
        NSString *curType = [types objectAtIndex:levelNum];
        int r = arc4random() % (curType.length);
        unichar c = [curType characterAtIndex:r];
        [output appendFormat:@"%C", c];
    }
    return output;
}

-(BOOL)isBlank
{
    if([[self stringByStrippingWhitespace] isEqualToString:@""])
        return YES;
    return NO;
}

-(BOOL)contains:(NSString *)string
{
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}

-(NSString *)stringByStrippingWhitespace
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSArray *)splitOnChar:(char)ch
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    int start = 0;
    for(int i=0; i<[self length]; i++) {
        BOOL isAtSplitChar = [self characterAtIndex:i] == ch;
        BOOL isAtEnd = i == [self length] - 1;
        if(isAtSplitChar || isAtEnd) {
            NSRange range;
            range.location = start;
            range.length = i - start + 1;
            if(isAtSplitChar)
                range.length -= 1;
            [results addObject:[self substringWithRange:range]];
            start = i + 1;
        }
        if(isAtEnd && isAtSplitChar)
            [results addObject:@""];
    }
    return results;
}

-(NSString *)substringFrom:(NSInteger)from to:(NSInteger)to {
    NSString *rightPart = [self substringFromIndex:from];
    return [rightPart substringToIndex:to-from];
}

@end
