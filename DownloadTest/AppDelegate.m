//
//  AppDelegate.m
//  DownloadTest
//
//  Created by Clique Studios on 1/28/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "AppDelegate.h"
#import "NSString+Extensions.h"
#import "FileHandler.h"
#import "SSZipArchive.h"

static inline NSString *downloadCacheDirectory()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSApplicationSupportDirectory, NSUserDomainMask, YES );
    NSString *dataDirectory = [ paths[0] stringByAppendingPathComponent:@"Deploy/zip-cache" ];
    return dataDirectory;
}

static inline NSString *coreCacheDirectory()
{
    return [downloadCacheDirectory() stringByAppendingPathComponent:@"core"];
}

@interface AppDelegate(Extensions)

@property (retain) NSURLConnection *connection;
@property (nonatomic, copy) void (^firstBlock)(NSString *message);
@property (nonatomic, copy) void (^secondBlock)(NSString *message);
@property (nonatomic, copy) void (^thirdBlock)(NSString *message);
/*
 @property (nonatomic, copy, setter = bk_setSuccessBlock:) void (^bk_successBlock)(NSURLConnection *connection, NSURLResponse *response, NSData *responseData);
 
  The block fired every time new data is sent to the server,
 representing the current percentage of completion.
 
 This block corresponds to the
 connection:didSendBodyData:totalBytesWritten:totalBytesExpectedToWrite:
 method of NSURLConnectionDelegate.
@property (nonatomic, copy, setter = bk_setUploadBlock:) void (^bk_uploadBlock)(double percent);

 The block fired every time new data is recieved from the server,
 representing the current percentage of completion.
 
 This block corresponds to the connection:didRecieveData:
 method of NSURLConnectionDelegate.
@property (nonatomic, copy, setter = bk_setDownloadBlock:) void (^bk_downloadBlock)(double percent);
*/

@end

@implementation AppDelegate
{
    NSString *downloadURL;
}

@synthesize downloadResponse;

- (IBAction)unzipWordpress:(id)sender
{
    NSString *zipPath = @"/Users/imnikkilee/Desktop/wordpress.zip";
    NSString *destinationPath = @"/Users/eriknielsen/Desktop/wordpress";
    [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
}

- (IBAction)downloadImage:(id)sender {
    
}

- (void)startDownload:(void (^)(void))block {
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://wordpress.org/latest.zip"]
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                            timeoutInterval:60.0];
    NSURLDownload  *theDownload = [[NSURLDownload alloc] initWithRequest:theRequest
                                                                delegate:self];
    if (!theDownload)
        return NSLog(@"Download Failed");
    if( block )
        block();
}

- (void)download:(NSURLDownload *)download decideDestinationWithSuggestedFilename:(NSString *)filename
{
    NSString *destinationFilename;
    NSString *destinationPath = @"/Users/eriknielsen/Desktop/wordpress";
    destinationFilename = [destinationPath stringByAppendingPathComponent:@"wordpress.zip"];
    [download setDestination:destinationFilename allowOverwrite:NO];
}

-(void)download:(NSURLDownload *)download didCreateDestination:(NSString *)path
{
    NSLog(@"Final file destination: %@",path);
}


- (void)download:(NSURLDownload *)download didFailWithError:(NSError *)error
{
    // Dispose of any references to the download object
    // that your app might keep.
    //...
    NSLog(@"Download failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)downloadDidFinish:(NSURLDownload *)download
{
    // Dispose of any references to the download object
    // that your app might keep.
    //...
    // Do something with the data.
    // NSLog(@"%@",@"downloadDidFinish");
//    [[[ZipArchive alloc] init] UnzipOpenFile:@"/Users/eriknielsen/Deesktop/wordpress.zip"];
}

- (void)setDownloadResponse:(NSURLResponse *)aDownloadResponse
{
    downloadResponse = aDownloadResponse;
}

- (void)download:(NSURLDownload *)download didReceiveResponse:(NSURLResponse *)response
{
    bytesReceived = 0;
    [self setDownloadResponse:response];
}

- (void)download:(NSURLDownload *)download didReceiveDataOfLength:(unsigned)length
{
    long long expectedLength = [[self downloadResponse] expectedContentLength];
    
    bytesReceived = bytesReceived + length;
    
    if (expectedLength != NSURLResponseUnknownLength) {
        float percentComplete = (bytesReceived/(float)expectedLength)*100.0;
        NSString *progressString = [@"Progress: " stringByAppendingFormat:@"%.02f", percentComplete];
        progressString = [progressString stringByAppendingString:@"%"];
        [progressTextField setStringValue:progressString];
//        [downloadProgress setIndeterminate:NO];
//        [downloadProgress startAnimation:self];
//        [downloadProgress setDoubleValue:percentComplete];
//        if(percentComplete < 100)
//           [downloadProgress setDoubleValue:percentComplete];
    } else {
        NSLog(@"Bytes received - %d", bytesReceived);
    }
}

@end

