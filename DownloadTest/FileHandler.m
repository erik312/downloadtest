//
//  FileHandler.m
//  DownloadTest
//
//  Created by Erik Nielsen on 2/3/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "FileHandler.h"

@implementation FileHandler

+ (BOOL)directoryExistsAtPath:(NSString *)aPath
{
    NSFileManager *fileHandler = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL dirExists = [fileHandler fileExistsAtPath:aPath isDirectory:&isDir];
    if(dirExists)
        if(isDir)
            return YES;
    return NO;
}

+ (void)createNewDirectory:(NSString *)dirPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    NSError *error;
    if([fileManager fileExistsAtPath:dirPath isDirectory:&isDir])
        return;
    [fileManager createDirectoryAtPath:dirPath
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:&error
     ];
    if(error != nil) {
        NSLog(@"Error creating directory: %@", error);
        return;
    }
    return;
}

@end
